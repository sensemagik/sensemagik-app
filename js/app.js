'use strict';
angular.module('mang', [
  'ui.bootstrap',
  'ui.router',
  'restangular',
  'ngResource',
  'mang.appConfig',
  'ngGrid',
  'ngRoute',
  'datePicker',
  'xeditable',
  'ui.calendar',
  'ngTable',
  'ngTableExport',
  'ngTagsInput',
  'angularFileUpload',
  'chart.js',
  'textAngular',
  'ngToast',
  'mwl.calendar',
  'angular-loading-bar',
  'jdFontselect',
  'ng-context-menu',
  'ui-notification',
  'ngDialog',
  'satellizer',
  'ngJoyRide',
  'cloudinary',
  'cgPrompt'
]).config([
  '$stateProvider',
  '$urlRouterProvider',
  '$sceDelegateProvider',
  'configProvider',
  'RestangularProvider',
  '$routeProvider',
  'cfpLoadingBarProvider',
  'NotificationProvider',
  '$authProvider',
  function ($stateProvider, $urlRouterProvider, $sceDelegateProvider, configProvider, RestangularProvider, $routeProvider, cfpLoadingBarProvider, NotificationProvider, $authProvider) {
    $urlRouterProvider.otherwise('/studio/list');
    NotificationProvider.setOptions({
      delay: 10000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'center',
      positionY: 'top'
    });
    $sceDelegateProvider.resourceUrlWhitelist([
     // Allow same origin resource loads.
     'self',
     // Allow loading from our assets domain.  Notice the difference between * and **.
     'https://res.cloudinary.com/**'
     ]);
  }
]).factory('APIRestangular', function (Restangular, config, $auth) {
  var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  handler.setDefaultHeaders({
    'api': config.api,
    'token': $auth.getToken()
  });
  handler.setBaseUrl('api/');
  return handler;
}).factory('VwsRestangular', function (Restangular, config) {
  var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  handler.setDefaultHeaders({ 'api': config.api });
  handler.setBaseUrl('vws/');
  return handler;
}).factory('GenRestangular', function (Restangular, config, $auth) {
  var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  handler.setDefaultHeaders({
    'token': $auth.getToken()
  });
  handler.setBaseUrl('api/');
  return handler;
}).run(function ($rootScope, $location, editableOptions, config) {
  // xeditable options
  editableOptions.theme = 'bs3';
  // storing user data fetched from backend in global variables
  $rootScope.username = config.username;
  $rootScope.useremail = config.useremail;
  $rootScope.userid = config.userid;
  $rootScope.plan = config.plan;
  //session management
  $rootScope.authStatus = true;
  // need to be false
  $rootScope.auth = {};
}).constant('jdFontselectConfig', { googleApiKey: 'AIzaSyD3BmGWe9nbGtfbAGWlMG2JLUilZeHqhyA' });