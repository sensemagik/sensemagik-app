angular.module('login', [
  'ui.bootstrap',
  'ui.router',
  'ngAnimate',
  'restangular',
  'ngRoute',
  'ngResource',
  'ui-notification',
  'satellizer',
  'login.appConfig',
  'mgcrea.ngStrap',
  'ngDialog',
  'angular-carousel'
]).config([
  '$stateProvider',
  '$urlRouterProvider',
  'RestangularProvider',
  '$routeProvider',
  'NotificationProvider',
  '$authProvider',
  'configProvider',
  function ($stateProvider, $urlRouterProvider, RestangularProvider, $routeProvider, NotificationProvider, $authProvider, configProvider) {
    NotificationProvider.setOptions({
      delay: 10000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'center',
      positionY: 'top',
      replaceMessage: true
    });
    $authProvider.google({
      url: '/auth/google',
      clientId: '723467182551-k40vq31mqah0cihs9dmhaqi90l6mm8iu.apps.googleusercontent.com',
      authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
      redirectUri: 'http://www.sensemagik.com/auth/google',
      scope: ['https://www.googleapis.com/auth/plus.login'],
      scopePrefix: 'openid',
      scopeDelimiter: ' ',
      requiredUrlParams: ['scope'],
      optionalUrlParams: ['display'],
      display: 'popup',
      type: '2.0',
      popupOptions: {
        width: 580,
        height: 400
      }
    });
    $authProvider.facebook({
      clientId: '899129386802712',
      url: '/auth/facebook',
      authorizationEndpoint: 'https://www.facebook.com/v2.3/dialog/oauth',
      redirectUri: 'http://www.sensemagik.com/auth/facebook',
      scope: 'email',
      scopeDelimiter: ',',
      requiredUrlParams: [
        'display',
        'scope'
      ],
      display: 'popup',
      type: '2.0',
      popupOptions: {
        width: 580,
        height: 400
      }
    });
  }
]).factory('APIRestangular', function (Restangular) {
  var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  handler.setBaseUrl('api/');
  return handler;
});
angular.module('login').controller('LoginController', [
  '$scope',
  '$http',
  '$rootScope',
  'APIRestangular',
  'Notification',
  '$window',
  '$auth',
  '$timeout',
  'Notification',
  '$modal',
  'config',
  '$aside',
  'ngDialog',
  function ($scope, $http, $rootScope, APIRestangular, Notification, $window, $auth, $timeout, Notification, $modal, config,$aside,ngDialog) {
    $scope.magik = {};
    $scope.basePath = config.basePath;
    $scope.aside = {
      "title": "Title",
      "content": "Hello Aside<br />This is a multiline message!"
    };
    $scope.openVideo = function(){
      ngDialog.open({template: 'watch-video'});
    }
    $scope.myInterval = 5000;
    $scope.noWrapSlides = true;
    var slides = $scope.slides = [{
      image: config.basePath+'img/gallery/santa.jpg'
    },{
      image:config.basePath+'img/gallery/avengers-badged.jpg'
    }];
    $scope.currentImage = $scope.slides[0].image;
    $scope.setCurrent = function(image){
      $scope.currentImage = image;
    }
    $scope.openGallery = function(){
      ngDialog.open({template: 'watch-gallery',scope: $scope});
    }
    /*
    $scope.magik.backgroundColors = [
      '#7B1111',
      'orange',
      '#064363',
      '#2F0535'
    ];*/
    //var index = Math.floor(Math.random() * 3) + 0;
    var index = 2;
    //$scope.magik.bg = $scope.magik.backgroundColors[index];  
   // $scope.magik.bg = "#064363";
    $scope.magik.bg = '#38033C';
    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };
    $scope.magik.authenticated = $scope.isAuthenticated();
    
    //console.log($scope.magik.authenticated);
    $scope.authenticate = function (provider) {
      Notification.info('Logging in.. Please Wait');
      //console.log('authenticating with ' + provider);
      $auth.authenticate(provider).then(function (response) {
        $window.location.reload();
      }).catch(function (response) {
      });
    };
    $rootScope.cancel = function () {
      //$('.login-body').removeClass('blurbg');
      $rootScope.modalInstance.dismiss('cancel');
    };
    $scope.login = function () {
      //console.log($scope.user);
      if($scope.user && $scope.user.email && $scope.user.password){
        $auth.login($scope.user).then(function () {
          Notification.warning('Loggin in ...');
          $window.location.reload();
        }).catch(function (response) {
          Notification.error('Please input correct email and password');
        });
      }else{
        Notification.error('Missing email or password');
      }
      
    };
    $scope.signup = function () {
      $scope.user = { 'email': $scope.magik.email };
      if($scope.magik.email){
          $auth.signup($scope.user).then(function (response) {
         // console.log(response);
          if (response.data == 'User Already Exists') {
            Notification.error('User Already Exists. Please login');
          } else {
            Notification.info('Signing in ..');
            $auth.setToken(response.data.token);
           // console.log($auth.getToken());
            $window.location.reload();
          }
        }).catch(function (response) {
          Notification.error('Unable to signup');
        });
      }else{
        Notification.warning('No Email address present');
      }
    };
    $scope.scrollTonext = function () {
      var target = $('#download');
      $('html, body').animate({ scrollTop: target.offset().top - '60' }, 1000);
    };
    $scope.subscribe = function (subscriber) {
     // console.log(subscriber);
      var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      var email = filter.test(subscriber);
      //console.log(email);
      if (!email) {
        Notification.error('Please enter valid email address');
      } else {
        var subscriberData = { 'email': subscriber };
        $http({
          method: 'POST',
          url: '/subscribe',
          data: $.param(subscriberData),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data) {
          if (data == 'duplicate') {
            Notification.warning('Email already subscribed');
          }
          if (data == 'success') {
            Notification.warning('Email subscribed successfully');
          }
        });
      }
    };
    $scope.forgetPassword = function () {
      var myModal= $modal({
        controller: 'LoginController',
        templateUrl: config.basePath+'forgetPassword.html', 
        show: false
      });
      myModal.$promise.then(myModal.show);
    };
    $scope.magik.resetPassword = function (email) {
      var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      var emailValid = filter.test(email);
      if (!emailValid) {
        Notification.error('Please enter valid email address');
      } else {
        var data = { 'email': email };
        $http({
          method: 'POST',
          url: '/resetpassword',
          data: $.param(data),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data) {
          if (data == 'nouser') {
            Notification.warning('Email address not registered. Please sign up');
          } else if (data == 'sent') {
            //Notification.warning('Link to change your password has been sent to your email');
            $scope.passwordResetSent = true;
          } else {
            Notification.error('Something went wrong! Please try after sometime');
          }
        });
      }
    };
    $scope.setBackground = function(color){
      $scope.magik.bg = color;
    };
    $scope.sendFeedback = function(feedback){
      $http({
        method: "POST",
        url: '/sendmail',
        data: $.param(feedback),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).success(function(data){
        $scope.feedbackSent=true;
      });
    }
  }
]);

angular.module('loginExtra', [
  'ui.bootstrap',
  'ui.router',
  'restangular',
]).config([
  '$stateProvider',
  '$urlRouterProvider',
  'RestangularProvider',
  function($stateProvider, $urlRouterProvider, RestangularProvider){

  }
]);
  
angular.module('loginExtra').controller('extraController',[
  '$scope',
  '$http',
  function($scope,$http){
    $scope.magik = {};
    $scope.sendFeedback = function(feedback){
      $http({
        method: "POST",
        url: '/sendmail',
        data: $.param(feedback),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).success(function(data){
        $scope.feedbackSent=true;
      });
    }
  }
]);
