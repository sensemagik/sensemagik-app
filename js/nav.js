angular.module('mang').controller('NavigationController', [
  '$scope',
  '$http',
  '$state',
  '$rootScope',
  '$location',
  function ($scope, $http, $state, $rootScope, $location) {
    $scope.mang = {};
    $scope.isActive = function (state) {
      var active = state === $rootScope.currentState;
      return active;
    };
    $scope.mang.navbar = [
      {
        name: 'Dashboard',
        state: 'studio',
        link: '/studio/list',
        classname: '',
        secured: 'yes'
      },
      {
        name: 'Tutorial',
        state:'tutorial',
        link: '/tutorial',
        secured: 'yes'
      },
      /*
      {
        name: 'Help Center',
        state: 'help',
        link: '/help',
        classname: '',
        secured: 'yes'
      },*/
      {
        name: 'FAQ',
        state: 'faq',
        link: '/faq',
        classname: '',
        secured: 'yes'
      }
    ];  
  }
]);