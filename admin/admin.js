'use strict';
angular.module('admin', [
  'ui.bootstrap',
  'ui.router',
  'restangular',
  'ngResource',
  'admin.appConfig',
  'ngGrid',
  'ngRoute',
  'datePicker',
  'xeditable',
  'ui.calendar',
  'ngTable',
  'ngTableExport',
  'ngTagsInput',
  'angularFileUpload',
  'chart.js',
  'textAngular',
  'ngToast',
  'mwl.calendar',
  'ngAnimate',
  'angular-loading-bar',
  'jdFontselect',
  'ng-context-menu',
  'wiz.validation',
  'ui-notification',
  'ngDialog',
  'satellizer'
  ]).config([
  '$stateProvider',
  '$urlRouterProvider',
  'configProvider',
  'RestangularProvider',
  '$routeProvider',
  'cfpLoadingBarProvider',
  'NotificationProvider',
  '$authProvider',
  '$sceDelegateProvider',
  function ($stateProvider, $urlRouterProvider, configProvider, RestangularProvider, $routeProvider, cfpLoadingBarProvider, NotificationProvider, $authProvider,$sceDelegateProvider) {
    //console.log(configProvider);
    //	console.log(cfpLoadingBarProvider);
    //cfpLoadingBarProvider.includeBar = true;
    $sceDelegateProvider.resourceUrlWhitelist([
     // Allow same origin resource loads.
     'self',
     // Allow loading from our assets domain.  Notice the difference between * and **.
     'https://res.cloudinary.com/**'
     ]);
    $urlRouterProvider.otherwise('/dashboard/stats');
    NotificationProvider.setOptions({
      delay: 10000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'center',
      positionY: 'top'
    });
  }
]).factory('APIRestangular', function (Restangular, config, $auth) {
  var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  handler.setDefaultHeaders({ 'api': config.api, 'token': $auth.getToken() });
  handler.setBaseUrl('api/');
  return handler;
}).factory('GenRestangular', function (Restangular, config, $auth) {
  var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  handler.setDefaultHeaders({
    'token': $auth.getToken()
  });
  handler.setBaseUrl('api/');
  return handler;
});