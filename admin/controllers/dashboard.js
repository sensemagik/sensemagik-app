'use strict';
angular.module('admin').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('dashboard', {
      url: '/dashboard',
      templateUrl: config.config.basePath + 'admin/views/dashboard.html',
      controller: 'DashboardController'
    }).state('dashboard.stats', {
      url: '/stats',
      templateUrl: config.config.basePath + 'admin/views/stats.html',
      controller: 'DashboardController'
    }).state('dashboard.users',{
      url: '/users',
      templateUrl: config.config.basePath + 'admin/views/users.html',
      controller: 'DashboardController'
    }).state('dashboard.pricing',{
      url: '/pricing',
      templateUrl: config.config.basePath + 'admin/views/pricing.html',
      controller: 'DashboardController'
    }).state('dashboard.templates',{
      url: '/templates',
      templateUrl: config.config.basePath + 'admin/views/templates.html',
      controller: 'DashboardController'
    }).state('dashboard.templatecategories',{
      url: '/templatecategories',
      templateUrl: config.config.basePath + 'admin/views/templatecategories.html',
      controller: 'DashboardController'
    }).state('dashboard.videotemplates',{
      url: '/videotemplates',
      templateUrl: config.config.basePath + 'admin/views/videotemplates.html',
      controller: 'VideoController'
    }).state('dashboard.videotemplatecategories',{
      url: '/videotemplatecategories',
      templateUrl: config.config.basePath + 'admin/views/videotemplatecategories.html',
      controller: 'VideoController'
    }).state('dashboard.invite',{
      url: '/invite',
      templateUrl: config.config.basePath + 'admin/views/invite.html',
      controller: 'InviteController'
    });
  }
]);
angular.module('admin').controller('InviteController',[
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  'ngTableParams',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  'FileUploader',
  function ($scope, $http, $state, APIRestangular, $rootScope, ngTableParams, $filter, $sce, $modal, $log, config, FileUploader) {
    
  }
]);
angular.module('admin').controller('DashboardController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  'ngTableParams',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  'FileUploader',
  function ($scope, $http, $state, APIRestangular, $rootScope, ngTableParams, $filter, $sce, $modal, $log, config, FileUploader) {
    $scope.basePath = config.basePath;
    $scope.magik = {
      users : {},
      templates: {},
      templatecategories: {},
      pricing: {},
      subscribers: {}
    };

    $scope.uploadImage = function () {
      $('#file').click();
    };

    // GET ALL USERS DATA
    APIRestangular.all('users').get('').then(function(response){
      $scope.magik.users = response;
    });
    //GET TEMPLATES
    APIRestangular.all('templates').getList().then(function(response){
      $scope.magik.templates = response;
     // console.log($scope.magik.templates);
    });

    // GET PRICING
    APIRestangular.all('pricing').getList().then(function(response){
      $scope.magik.pricing = response[0];
      if($scope.magik.pricing){
        $scope.magik.pricingisPresent = true;
      }
      $scope.magik.pricinginitial = $scope.magik.pricing.length;
      console.log($scope.magik.pricing);
    });

    $http({
      method: 'GET',
      url: '/subscribe',
    }).success(function(data){
      console.log(data);
      $scope.magik.subscribers = data;
    });

    $scope.loadTemplates = function(){
      APIRestangular.all('templates').getList().then(function(response){
        $scope.magik.templates = response;
      });
    };

    //GET TEMPLATE CATEGORIES
    APIRestangular.all('templatecategories').getList().then(function (data) {
      $scope.magik.templatecategories = data;
    //  console.log(data);
      $scope.tableParams = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
          name: 'desc'  // initial sorting
        }
      }, {
        total: $scope.magik.templatecategories.length,
        // length of data
        getData: function ($defer, params) {
          //////console.log($scope.mang.projectsList);
          var orderedData = params.sorting() ? $filter('orderBy')($scope.magik.templatecategories, params.orderBy()) : $scope.magik.templatecategories;
          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          $defer.resolve($scope.magik.templatecategories.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          params.total($scope.magik.templatecategorieslength);
        }
      });
    });

    //Templates
    $scope.uploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      autoUpload: true,
      formData: [{
            upload_preset: config.uploadPreset,
            tags: 'admin'
      }
      ],
      url: config.uploadUrl
    });
    $scope.uploader.onProgressAll = function (progress) {
      $rootScope.isAnalysingTemplate = true;
      $rootScope.templateAnalysisProgress = progress;
    };
    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
      $rootScope.isAnalysingTemplate = false;
      $rootScope.modalInstance.dismiss('ok');
      $scope.datum = {};
      $scope.datum.public_id = response.public_id;
      $scope.datum.name = response.public_id;
      $scope.datum.url = response.secure_url;
      APIRestangular.all('templates').post($scope.datum).then(function (response) {
        console.log(response);
        $scope.loadTemplates();
      });
    };

    $scope.addTemplateModal = function () {
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'admin/views/addTemplatesModal.html',
        scope: $scope,
        controller: 'DashboardController',
        size: 'lg'
      });
    };

    $scope.editTemplate = function (data) {
      console.log(data);
      data.put().then(function (response) {
        console.log(response);
      });
    };

    $scope.deleteTemplate = function (data) {
      data.remove().then(function (response) {
        $scope.loadTemplates();
      });
    };



    //Template category

    $scope.addCategory = function (data, icon) {
      console.log(data);
      var dat = {};
      dat.name = data;
      dat.icon = icon;
      APIRestangular.all('templatecategories').post(dat).then(function (response) {
        console.log(response);
      });
    };

    $scope.editCategory = function (data) {
      console.log(data);
      delete data.$edit;
      data.put().then(function (response) {
      });
    };

    $scope.deleteCategory = function(data){
      data.remove().then(function(response){
        console.log(response);
      })
    };

    //PRICING
    $scope.addPricing = function(){
      console.log($scope.magik.pricing);
      APIRestangular.all('pricing').post($scope.magik.pricing).then(function(response){
        console.log(response);
      },function(error){
        console.log(error);
      })
    }

    $scope.savePricing = function(){
      console.log($scope.magik.pricinginitial);
      
      $scope.magik.pricing.put().then(function(response){
        console.log(response);
      },function(error){
        console.log(error);
      });
    };

    $scope.showUserData = function(user){
      $scope.userData = user;
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'admin/views/userDetail.html',
        scope: $scope,
        controller: 'DashboardController',
        size: 'lg'
      });
    }
  }
]);