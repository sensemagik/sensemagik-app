'use strict';
angular.module('admin').controller('VideoController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  'ngTableParams',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  'FileUploader',
  'GenRestangular',
  function ($scope, $http, $state, APIRestangular, $rootScope, ngTableParams, $filter, $sce, $modal, $log, config, FileUploader,GenRestangular) {
    $scope.basePath = config.basePath;

    $scope.magik = {
      videotemplates: {},
      videotemplatecategories: {}
    }

    //GET TEMPLATES
    GenRestangular.all('videotemplates').getList().then(function(response){
      $scope.magik.videotemplates = response;
      console.log($scope.magik.videotemplates);
    });

    $scope.loadTemplates = function(){
      GenRestangular.all('videotemplates').getList().then(function(response){
        $scope.magik.videotemplates = response;
      });
    };

    //GET TEMPLATE CATEGORIES
    GenRestangular.all('videotemplatecategories').getList().then(function (data) {
      $scope.magik.videotemplatecategories = data;
      console.log(data);
      $scope.tableParamss = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
          name: 'desc'  // initial sorting
        }
      }, {
        total: $scope.magik.videotemplatecategories.length,
        // length of data
        getData: function ($defer, params) {
          //////console.log($scope.mang.projectsList);
          var orderedData = params.sorting() ? $filter('orderBy')($scope.magik.videotemplatecategories, params.orderBy()) : $scope.magik.videotemplatecategories;
          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          $defer.resolve($scope.magik.videotemplatecategories.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          params.total($scope.magik.videotemplatecategories);
        }
      });
    });

    //Templates
    $scope.uploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      autoUpload: true,
      formData: [{
            upload_preset: config.uploadPreset,
            tags: 'admin'
      }
      ],
      url: config.uploadUrl
    });
    $scope.uploader.onProgressAll = function (progress) {
      $rootScope.isAnalysingTemplate = true;
      $rootScope.analysisProgress = progress;
      $scope.$apply();
    };
    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
      $rootScope.isAnalysingTemplate = false;
      $rootScope.modalInstance.dismiss('ok');
      console.log(response);
      $scope.datum = {};
      $scope.datum.public_id = response.public_id;
      $scope.datum.name = response.public_id;
      $scope.datum.url = response.secure_url;
      GenRestangular.all('videotemplates').post($scope.datum).then(function (response) {
        console.log(response);
        $scope.loadTemplates();
      });
    };

    $scope.addVideoTemplateModal = function () {
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'admin/views/addTemplatesModal.html',
        scope: $scope,
        controller: 'VideoController',
        size: 'lg'
      });
    };

    $scope.editTemplate = function (data) {
      console.log(data);
      data.put().then(function (response) {
        console.log(response);
      });
    };

    $scope.deleteTemplate = function (data) {

      console.log(data);
      /*
      data.remove().then(function (response) {
        console.log(response);
      });*/
      $http({
        method: 'DELETE',
        url: 'api/videotemplates/'+data._id
      }).success(function(response){
        console.log(response);
      })
    };
    
    //Template category
    $scope.addCategory = function (data, icon) {
      console.log(data);
      var dat = {};
      dat.name = data;
      dat.icon = icon;
      GenRestangular.all('videotemplatecategories').post(dat).then(function (response) {
        console.log(response);
      });
    };

    $scope.editCategory = function (data) {
      console.log(data);
      delete data.$edit;
      data.put().then(function (response) {
      });
    };

    $scope.deleteCategory = function(id){
      console.log(id);
      var data = {};
      data = $filter('filter')($scope.magik.videotemplatecategories, function (d) {
        return d._id == id;
      })[0];
      console.log(data);
      data.remove().then(function(response){
        console.log(response);
      });
    };
  }
]);