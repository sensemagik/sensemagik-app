angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('members', {
      url: '/members',
      templateUrl: config.config.basePath + 'modules/members/views/members.html',
      controller: 'MembersController'
    }).state('members.new', {
      url: '/new',
      templateUrl: config.config.basePath + 'modules/members/views/new.html',
      controller: 'MembersController'
    }).state('members.edit', {
      url: '/edit/:id',
      templateUrl: config.config.basePath + 'modules/members/views/edit.html',
      controller: 'MembersEditController'
    }).state('members.list', {
      url: '/list',
      templateUrl: config.config.basePath + 'modules/members/views/list.html',
      controller: 'MembersController'
    });
  }
]);
angular.module('mang').controller('MembersController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  '$location',
  'config',
  'FileUploader',
  function ($scope, $http, $state, APIRestangular, $rootScope, $location, config, FileUploader) {
    console.log('MembersController page');
    $rootScope.currentState = 'members';
    $scope.basePath = config.basePath;
    if ($rootScope.custom_fields) {
      $rootScope.custom_fields.length = 0;
    }
    $scope.user = {};
    $scope.role = config.userrole;
    if ($scope.role === 'admin') {
      $scope.admin = true;
    }
    console.log($state.current.name);
    $scope.profilepic = config.basePath + 'img/user_profile.jpg';
    if ($state.current.name == 'members') {
      $state.go('members.list');
    }
    $scope.openFileBrowser = function () {
      console.log('clicked');
      $('#file').click();
    };
    $scope.uploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      autoUpload: true,
      url: 'blob/photo'
    });
    $scope.uploader.onAfterAddingFile = function (item) {
      console.log('file added');
      console.log(item);
    };
    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
      console.log(response);
      console.log(fileItem);
      $scope.profilepic = response;
    };
    $scope.mang = {};
    $scope.mang.usersList = {};
    APIRestangular.all('users').get('').then(function (response) {
      $scope.mang.usersList = response;
    });
    $scope.saveUser = function () {
      $scope.user.profilepic = $scope.profilepic;
      APIRestangular.all('users').post($scope.user).then(function (response) {
        console.log(response);
      });
    };
    $scope.deleteMember = function () {
    };
  }
]);
angular.module('mang').controller('MembersEditController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  'ngTableParams',
  '$filter',
  '$sce',
  'FileUploader',
  'config',
  function ($scope, $http, $state, APIRestangular, $rootScope, ngTableParams, $filter, $sce, FileUploader, config) {
    console.log($state.params);
    if ($rootScope.custom_fields) {
      $rootScope.custom_fields.length = 0;
    }
    $scope.user = {};
    $scope.profilepic = config.basePath + 'img/user_profile.jpg';
    $scope.uploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      autoUpload: true,
      url: 'blob/photo'
    });
    $scope.uploader.onAfterAddingFile = function (item) {
      console.log('file added');
      console.log(item);
    };
    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
      console.log(response);
      console.log(fileItem);
      $scope.profilepic = response;
    };
    APIRestangular.all('users').get($state.params.id).then(function (response) {
      console.log(response);
      $scope.user = response;
      if ($scope.user.profilepic) {
        $scope.profilepic = $scope.user.profilepic;
      }
    });
    $scope.editUser = function () {
      $scope.user.profilepic = $scope.profilepic;
      $scope.user.put().then(function (response) {
        console.log(response);
      });
    };
  }
]);