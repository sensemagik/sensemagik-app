angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('profile', {
      url: '/profile',
      templateUrl: config.config.basePath + 'modules/profile/views/profile.html',
      controller: 'ProfileController'
    }).state('profile.notification', {
      url: '/notification',
      templateUrl: config.config.basePath + 'modules/profile/views/notification.html',
      controller: 'ProfileController'
    }).state('profile.view', {
      url: '/view',
      templateUrl: config.config.basePath + 'modules/profile/views/view.html',
      controller: 'ProfileController'
    });
  }
]);
angular.module('mang').controller('ProfileController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  '$location',
  'config',
  'FileUploader',
  '$modal',
  function ($scope, $http, $state, APIRestangular, $rootScope, $location, config, FileUploader, $modal) {
    $rootScope.currentState = 'profile';
    $scope.user = config.userid;
    $scope.magik = {};
    $scope.basePath = config.basePath;
    $scope.profile = {};
    $scope.profileOriginal = {};
    $scope.$on('$viewContentLoaded', function(event){
      $('body').removeClass('blurbg');
    });
    $rootScope.cancel = function () {
      $('.content-page').removeClass('blurbg');
      $rootScope.modalInstance.dismiss('cancel');
    };
    if ($state.current.name == 'profile') {
      $state.go('profile.view');
    }
    APIRestangular.all('users').get($scope.user).then(function (response) {
      $scope.profile = response;
      $scope.profileOriginal = angular.copy($scope.profile);
    });
    $scope.openFileBrowser = function () {
      $('#file').click();
    };
    $scope.uploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      formData: [{
            upload_preset: config.uploadPreset,
            tags: 'profilepic'
      }
      ],
      url: config.uploadUrl
    });
    $scope.uploader.onAfterAddingFile = function (file) {
      if (file.uploader.queue[0].file.type == 'image/jpeg' || file.uploader.queue[0].file.type == 'image/png') {
        var deletePublicId = {
          public_id: $scope.profile.public_id
        }
        if(deletePublicId){
          APIRestangular.all('cloudinary').post(deletePublicId).then(function(response){
            $scope.uploader.queue[0].upload();
            $rootScope.isAnalysing = true;
          });
        }else{
          $scope.uploader.queue[0].upload();
          $rootScope.isAnalysing = true;
        }
        
        
      } else {
        alert('File Not supported');
      }
    };
    $scope.uploader.onProgressAll = function (progress) {
      $rootScope.analysisProgress = progress;
      $scope.$apply();
    };
    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
      $scope.profilepic = response;
      $scope.profile.profilepic = response.secure_url;
      $scope.profile.public_id = response.public_id;
      $scope.profile.put().then(function (response) {
        $rootScope.isAnalysing = false;
        $scope.uploader.clearQueue();
      });
    };
    $scope.saveProfile = function () {
      $scope.profile.put().then(function(response){
      });
    };
    $scope.reset = function(){
      $scope.profile = angular.copy($scope.profileOriginal);
    }
    $scope.openFile = function () {
      $('#file').click();
    };
    $scope.changeProfilePic = function () {
      $('.content-page').addClass('blurbg');
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'modules/profile/views/profilePicModal.html',
        size: 'lg',
        controller: 'ProfileController',
        backdrop: true
      });
    };
  }
]);