'use strict';
angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('pricing', {
      url: '/pricing',
      templateUrl: config.config.basePath + 'modules/pricing/views/main.html',
      controller: 'PricingController'
    });
  }
]);
angular.module('mang').controller('PricingController', [
  '$scope',
  '$http',
  '$state',
  '$rootScope',
  'config',
  function ($scope, $http, $state, $rootScope, config) {
    $rootScope.currentState = 'pricing';
    $scope.$on('$viewContentLoaded', function(event){
      console.log('content loaded!');
      $('body').removeClass('blurbg');
    });
    $scope.basePath = config.basePath;
    console.log('pricing');
  }
]);