'use strict';
angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('help', {
      url: '/help',
      templateUrl: config.config.basePath + 'modules/help/views/help.html',
      controller: 'HelpController'
    });
  }
]);
angular.module('mang').controller('HelpController', [
  '$scope',
  '$http',
  '$state',
  '$rootScope',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  '$location',
  'Notification',
  function ($scope, $http, $state, $rootScope, $filter, $sce, $modal, $log, config, $location, Notification) {
    $rootScope.currentState = 'help';
    console.log(config);
    $scope.basePath = config.basePath;
    console.log($scope.basePath);
    $scope.$on('$viewContentLoaded', function(event){
      console.log('content loaded!');
      $('body').removeClass('blurbg');
    });
  }
]);