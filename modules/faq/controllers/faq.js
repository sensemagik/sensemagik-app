'use strict';
angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('faq', {
      url: '/faq',
      templateUrl: config.config.basePath + 'modules/faq/views/faq.html',
      controller: 'FaqController'
    });
  }
]);
angular.module('mang').controller('FaqController', [
  '$scope',
  '$http',
  '$state',
  '$rootScope',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  '$location',
  'Notification',
  function ($scope, $http, $state, $rootScope, $filter, $sce, $modal, $log, config, $location, Notification) {
    $rootScope.currentState = 'faq';
    $scope.$on('$viewContentLoaded', function(event){
      $('body').removeClass('blurbg');
    });
    $scope.basePath = config.basePath;
    $scope.oneAtATime = true;
    $scope.groups = [
      {
        title: 'Dynamic Group Header - 1',
        content: 'Dynamic Group Body - 1'
      },
      {
        title: 'Dynamic Group Header - 2',
        content: 'Dynamic Group Body - 2'
      }
    ];
    $scope.items = [
      'Item 1',
      'Item 2',
      'Item 3'
    ];
    $scope.addItem = function () {
      var newItemNo = $scope.items.length + 1;
      $scope.items.push('Item ' + newItemNo);
    };
  }
]);