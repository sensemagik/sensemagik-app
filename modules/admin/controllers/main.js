'use strict';
angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('admin', {
      url: '/admin',
      templateUrl: config.config.basePath + 'modules/admin/views/admin.html',
      controller: 'AdminController'
    }).state('admin.dashboard', {
      url: '/dashboard',
      templateUrl: config.config.basePath + 'modules/admin/views/dashboard.html',
      controller: 'AdminController'
    });
  }
]);
angular.module('mang').controller('AdminController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  'ngTableParams',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  'FileUploader',
  function ($scope, $http, $state, APIRestangular, $rootScope, ngTableParams, $filter, $sce, $modal, $log, config, FileUploader) {
    $rootScope.currentState = 'admin';
    $scope.magik = {};
    $scope.magik.templates = {};
    $scope.magik.templateCategories = {};
    APIRestangular.all('templatecategories').getList().then(function (data) {
      $scope.magik.templateCategories = data;
      console.log(data);
      $scope.tableParams = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
          name: 'desc'  // initial sorting
        }
      }, {
        total: $scope.magik.templateCategories.length,
        // length of data
        getData: function ($defer, params) {
          ////console.log($scope.mang.projectsList);
          var orderedData = params.sorting() ? $filter('orderBy')($scope.magik.templateCategories, params.orderBy()) : $scope.magik.templateCategories;
          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          $defer.resolve($scope.magik.templateCategories.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          params.total($scope.magik.templateCategorieslength);
        }
      });
    });
    $scope.editCategory = function (data) {
      console.log(data);
      delete data.$edit;
      data.put().then(function (response) {
      });
    };
    $scope.editTemplate = function (data) {
      console.log(data);
      data.put().then(function (response) {
        console.log(response);
      });
    };
    $scope.deleteTemplate = function (data) {
      data.remove().then(function (response) {
        console.log(response);
      });
    };
    $scope.loadTemplates = function () {
      APIRestangular.all('templates').getList().then(function (data) {
        // console.log(data);
        $scope.magik.templates = data;
      });
    };
    $scope.loadTemplates();
    $rootScope.cancel = function () {
      $rootScope.modalInstance.dismiss('cancel');
    };
    $scope.uploadImage = function () {
      $('#file').click();
    };
    $scope.uploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      autoUpload: true,
      url: 'blob/photo'
    });
    $scope.uploader.onProgressAll = function (progress) {
      $rootScope.isAnalysingTemplate = true;
      $rootScope.templateAnalysisProgress = progress;
    };
    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
      $rootScope.isAnalysingTemplate = false;
      $rootScope.modalInstance.dismiss('ok');
      $scope.datum = {};
      $scope.datum.name = fileItem.uploader.queue[0].file.name;
      $scope.datum.url = response;
      APIRestangular.all('templates').post($scope.datum).then(function (response) {
        console.log(response);
        $scope.loadTemplates();
      });
    };
    $scope.addTemplateModal = function () {
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'modules/admin/views/addTemplatesModal.html',
        scope: $scope,
        controller: 'AdminController',
        size: 'lg'
      });
    };
    $scope.addCategory = function (data, icon) {
      console.log(data);
      var dat = {};
      dat.name = data;
      dat.icon = icon;
      APIRestangular.all('templatecategories').post(dat).then(function (response) {
        console.log(response);
      });
    };
  }
]);