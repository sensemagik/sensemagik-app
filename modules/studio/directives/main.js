angular.module('mang').directive('adjustSlider', function () {
  return {
    link: function (scope, element, attrs) {
      var value=attrs.adjustSlider;
      $(element).slider({
        range: 'min',
        min: 10,
        max: 100,
        value: value || 50,
        slide: function (event, ui) {
          scope.magik.proportion = ui.value / 100;
          scope.$apply();
        }
      });
    }
  };
});
angular.module('mang').directive('loadTop',function(){
  return{
    link:function(scope,element,attrs){
      console.log('Load');
      $(element).ready(function(){
        $(this).scrollTop(0);
      })
    }
  }
})