'use strict';
angular.module('mang').controller('ImageTemplateController',[
  '$scope',
  '$rootScope',
  '$state',
  'APIRestangular',
  '$filter',
  '$sce',
  'config',
  function($scope,$rootScope,$state,APIRestangular,$filter,$sce,config){
      //console.log('model opened');
    }
]);
angular.module('mang').controller('EditorPageController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  'GenRestangular',
  '$rootScope',
  'ngTableParams',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  'FileUploader',
  '$location',
  'VwsRestangular',
  'ngDialog',
  '$window',
  function ($scope, $http, $state, APIRestangular,GenRestangular, $rootScope, ngTableParams, $filter, $sce, $modal, $log, config, FileUploader, $location, VwsRestangular, ngDialog, $window) {
    //console.log('editpr page controller');
    $rootScope.currentState = 'studio';
    //open tutorial box onload
    //ngDialog.open({ template: config.basePath+'modules/studio/views/tutor.html' });
    //console.log($scope);
    $scope.magik = {};
    $scope.magik.proportion = '1';
    $scope.basePath = config.basePath;
    $scope.magik.templates = {};
    $scope.local = {};
    $scope.local.magikTarget = {};
    $scope.localImageSize = {};
   // var url = document.URL;
   // $scope.local.magikTarget.id = url.substring(url.lastIndexOf('/') + 1);
   $scope.local.magikTarget.id = $state.params.id;
    //console.log($scope.local.magikTarget.id);
    // //console.log($scope.magikTarget.id);
    $scope.magik.videos = [];
    $scope.magik.videotemplatecategories = {};
    $scope.magik.videotemplates = {};
    $scope.magik.targetcategories = {};
    $scope.magik.gallery = {};
    $scope.magik.user = {};
    $scope.magik.currentpricing = {};

    $scope.preloader= config.basePath+'img/login/preloader.gif';
   

    $scope.magik.uploadImage = function () {
      //console.log('upload image clicked');
      //console.log($('#file'));
      $('#file').trigger('click');

    };
    $scope.$on('$viewContentLoaded', function(event){
      ////console.log('content loaded!');
      $('body').removeClass('blurbg');
    });
    APIRestangular.all('users').get(config.userid).then(function (data) {
      $scope.magik.user = data;
      //console.log(data);
    }, function (error) {
      //console.log(error);
    });
    APIRestangular.all('gallery').getList({}).then(function (data) {
      //console.log(data);
      $scope.magik.gallery = data;
    });
    VwsRestangular.all('targets').getList({}).then(function (response) {
      ////console.log(response);
    });
    APIRestangular.all('videos').getList({}).then(function (response) {
     // //console.log(response);
      $scope.magik.videos = response;
    });
    
    GenRestangular.all('templates').getList({}).then(function (response) {
      $scope.magik.templates = response;
      //console.log(response);
    });
    GenRestangular.all('templatecategories').getList({}).then(function (data) {
      $scope.magik.templatecategories = data;
     // //console.log(data);
    });
    GenRestangular.all('videotemplates').getList({}).then(function (response) {
      $scope.magik.videotemplates = response;
      //console.log(response);
    });
    GenRestangular.all('videotemplatecategories').getList({}).then(function (data) {
      $scope.magik.videotemplatecategories = data;
     // //console.log(data);
    });
    
    APIRestangular.all('pricing').getList({}).then(function (data) {
      var pricing = {};
      pricing = data[0];
      $scope.currentPlan = $scope.magik.user.plan;
      $scope.magik.currentpricing = pricing[$scope.currentPlan];
      //console.log($scope.magik.currentpricing);
    });
    $scope.augmenttypeChanged = function () {
      $scope.local.magikTarget.put().then(function (response) {
      ////console.log(response);
      });
    };
    $scope.videouploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      autoUpload: true,
      formData: [{
            upload_preset: config.uploadPreset,
            tags: 'videos'
      }
      ],
      url: config.uploadUrl
    });
    $scope.videouploader.onProgressAll = function (progress) {
      $rootScope.isAnalysingVideo = true;
      $rootScope.videoAnalysisProgress = progress;
      //$scope.$apply();
    };
    $scope.videouploader.onCompleteItem = function (fileItem, response, status, headers) {
      $scope.videos = {};
      $scope.videos.api = config.api;
      $scope.videos.public_id = response.public_id;
      $scope.videos.name = response.public_id;
      $scope.videos.url = response.secure_url;
      APIRestangular.all('videos').post($scope.videos).then(function (response) {
        ////console.log(response);
        $rootScope.isAnalysingVideo = false;
        APIRestangular.all('videos').getList().then(function (response) {
          $scope.magik.videos = response;
          $scope.videouploader.clearQueue();
        });

      });
    };
    $scope.imageuploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      autoUpload: true,
      formData: [{
            upload_preset: config.uploadPreset,
            tags: 'image'
      }
      ],
      url: config.uploadUrl
    });
    $scope.imageuploader.onProgressAll = function (progress) {
      $rootScope.isAnalysingImage = true;
      $rootScope.ImageAnalysisProgress = progress;
      //console.log(progress);
      //$scope.$apply();
    };
    $scope.imageuploader.onCompleteItem = function (fileItem, response, status, headers) {
      ////console.log(fileItem);
      $scope.gallery = {};
      $scope.gallery.public_id = response.public_id;
      $scope.gallery.name = response.public_id;
      $scope.gallery.url = response.secure_url;
      $scope.gallery.api = config.api;
      APIRestangular.all('gallery').post($scope.gallery).then(function (response) {
        ////console.log(response);
        $rootScope.isAnalysingImage = false;
        APIRestangular.all('gallery').getList().then(function (data) {
          $scope.magik.gallery = data;
          //console.log($scope.magik.gallery);
          $scope.imageuploader.clearQueue();
        });
      });
    };
    APIRestangular.all('targets').get($scope.local.magikTarget.id).then(function (response) {
      
      $scope.local.magikTarget = response;
      $scope.getImageData();
    });
    
    $scope.getImageData = function () {
      
      //console.log($('#slider').value);
      var img = new Image();
      img.onload = function () {
        //console.log( this.width+' '+ this.height );
        $scope.localImageSize.width = this.width;
        $scope.localImageSize.height = this.height;
        if($scope.localImageSize.width >= '1000'){
          $scope.magik.proportion = '.5';
          $scope.magik.siderPosition = 50;
        }
        $scope.backgroundLoaded = true;
        $scope.$apply();
      };
      //console.log('image load fro first '+$scope.local.magikTarget.imgurl);
      img.src = $scope.local.magikTarget.imgurl;  // //console.log($scope.localImageSize);
    };
    $rootScope.cancel = function () {
      $('.editor-body').removeClass('blurbg');
      $rootScope.modalInstance.dismiss('cancel');
    };
    // //console.log($scope.imgurl);
    $scope.browseTemplates = function () {
      $('.editor-body').addClass('blurbg');
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'modules/studio/views/templatesModal.html',
        controller: 'ImageTemplateController',
        size: 'lg',
        scope: $scope,
        backdrop: false
      });
    };
    $scope.insertTemplate = function (data) {
      ////console.log(data);
      $rootScope.modalInstance.dismiss('ok');
      ////console.log($scope.local.magikTarget);
      $('.editor-body').removeClass('blurbg');
      ////console.log($scope.magik.imagehotspotdata);
      $scope.local.magikTarget.imagehotspot = {};
      $scope.local.magikTarget.imagehotspot.url = data.url;
      $scope.local.magikTarget.imagehotspot.left = "0%";
      $scope.local.magikTarget.imagehotspot.top = "0%";
      $scope.local.magikTarget.imagehotspot.height = "10%";
      $scope.local.magikTarget.imagehotspot.width = "20%";
      $scope.local.magikTarget.put().then(function (response) {
      });
    };
    $scope.removeTemplate = function () {
      //console.log('remove template');
      //console.log($scope.local.magikTarget);
      delete $scope.local.magikTarget.imagehotspot;
      $scope.local.magikTarget.put().then(function (response) {
        //console.log(response);
      });
    };
    $scope.uploadVideo = function () {
      $('#videofile').click();
    };
    
    $scope.openVideoModal = function () {
      $('.editor-body').addClass('blurbg');
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'modules/studio/views/videoGalleryModal.html',
        scope: $scope,
        size: 'lg',
        backdrop: false
      });
    };
    
    $scope.addVideoHotspot = function (data) {
      //console.log(data);
      $rootScope.modalInstance.dismiss('ok');
      $('.editor-body').removeClass('blurbg');
      $scope.local.magikTarget.videohotspot = {};
      $scope.local.magikTarget.videohotspot.url = data.url;
      $scope.local.magikTarget.videohotspot.public_id = data.public_id;
      $scope.local.magikTarget.videohotspot.transparent = (data.transparency)?true:false;
      /*
      $scope.local.magikTarget.videohotspot = {
        'id': data._id,
        'url': data.url
      };*/
      $scope.local.magikTarget.put().then(function (response) {
        //console.log(response);
      });
    };

    $scope.openTextEditor = function () {
      $('.editor-body').addClass('blurbg');
      $rootScope.modalInstance = $modal.open({
        templateUrl: config.basePath + 'modules/studio/views/textEditorModal.html',
        scope: $scope,
        size: 'lg',
        backdrop: false
      });
    };
    $scope.addTextHotspot = function (data) {
      $('.editor-body').removeClass('blurbg');
      if (!$scope.local.magikTarget.texthotspots) {
        $scope.local.magikTarget.texthotspots = {};
      }
      $scope.local.magikTarget.texthotspots.value = data;
      $scope.local.magikTarget.put().then(function (response) {
        //console.log(response);
        $rootScope.modalInstance.dismiss('ok');
        $scope.textHotspotAdded = true;
        //$rootScope.textHotspotData = data;
        
      });
    };

    $scope.removeTextHotspot = function(){
      //console.log('remove text hotspot');
      //console.log($scope.local.magikTarget);
      delete $scope.local.magikTarget.texthotspots;
      $scope.local.magikTarget.put().then(function (response) {
        //console.log(response);
      });
    }

    

    $scope.publish = function(data){
      //console.log(data);
      $scope.$parent.magik.publish(data);
      $state.go('studio.list');
    };
  }
]);