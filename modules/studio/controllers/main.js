'use strict';
angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('studio', {
      url: '/studio',
      templateUrl: config.config.basePath + 'modules/studio/views/studio.html',
      controller: 'StudioController'
    }).state('studio.new', {
      url: '/new',
      templateUrl: config.config.basePath + 'modules/studio/views/new.html',
      controllerAs: 'StudioController'
    }).state('studio.analysis', {
      url: '/analysis',
      templateUrl: config.config.basePath + 'modules/studio/views/analysis.html',
      controllerAs: 'StudioController'
    }).state('studio.list', {
      url: '/list',
      templateUrl: config.config.basePath + 'modules/studio/views/list.html',
      controllerAs: 'StudioController'
    }).state('studio.edit',{
      url:'/edit/:id',
      templateUrl: config.config.basePath + 'modules/studio/views/edit.html',
      //templateUrl: config.config.basePath + 'editor.html',
      controller: 'EditorPageController'
    });
  }
]);
angular.module('mang').controller('StudioController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  '$rootScope',
  'ngTableParams',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  'FileUploader',
  '$location',
  '$window',
  'VwsRestangular',
  'Notification',
  '$timeout',
  'ngDialog',
  '$auth',
  'prompt',
  function ($scope, $http, $state, APIRestangular, $rootScope, ngTableParams, $filter, $sce, $modal, $log, config, FileUploader, $location, $window, VwsRestangular, Notification, $timeout, ngDialog,$auth,prompt) {
    $rootScope.currentState = 'studio';
    //console.log($modal);
    $scope.listStyle = 'box';
    $scope.basePath = config.basePath;
    $scope.magik = {};
    $scope.magik.targets = {};
    $scope.magik.user = {};
    $scope.magik.currentpricing = {};
    //console.log('executing time ');
    $scope.$on('$viewContentLoaded', function(event){
      //console.log('content loaded!');
      $(".se-pre-con").fadeOut("slow");
    });
      
    $scope.viewTarget = function(imgurl,public_id){
      ngDialog.open({
        template: 'view-target',
        data: {
          "img":imgurl,
          "public_id":public_id
        }
      });
    }

    $scope.magik.fetchId = function (resp) {
      var headers = resp.headers();
      if (headers.location) {
        var items = headers.location.split('/');
        var newId = items[items.length - 1];
        return newId;
      }
      return 0;
    };

    function _generateTextForNext(isEnd) {
      if (isEnd) {
        return 'Finish';
      } else {
        return 'Next';
      }
    }
    function elementTourTemplate(content, isEnd) {
      return '<div class="row"><div id="pop-over-text" class="col-md-12">' + content + '</div></div><hr><div class="row"><div class="col-md-4 center"><a class="skipBtn pull-left" type="button">Skip</a></div><div class="col-md-8"><div class="pull-right"><button id="prevBtn" class="prevBtn btn btn-xs" type="button">Previous</button> <button id="nextBtn" class="nextBtn btn btn-xs btn-primary" type="button">' + _generateTextForNext(isEnd) + '</button></div></div></div>';
    }
    $scope.config = [{
        type: 'title',
        heading: 'Welcome to the Studio',
        text: '<div class="row"><div id="title-text" class="col-md-12"><div class>We will take you through our Studio</div></div>'
      }];
    //$scope.startJoyRide = true;
    
    APIRestangular.all('users').get(config.userid).then(function (data) {
      $scope.magik.user = data;
      if($scope.magik.user.firstlogin == true){
        ngDialog.open({ template: 'enter-password' ,scope: $scope });
      }
    }, function (error) {
      //console.log(error);
    });
    $scope.savePassword = function(password){
      console.log("pas"+password);
      $scope.magik.user.password = password;
      $scope.magik.user.firstlogin = false;
      $scope.magik.user.put().then(function(response){
        ngDialog.close({template: 'enter-password'});
        ngDialog.open({template: 'password-saved'});
      })
    };
    $scope.resendActivate = function(){
      var user = {
        email : $scope.magik.user.email,
        token: $scope.magik.user.token
      }
      $http({
          method: 'POST',
          url: '/resendactivate',
          data: $.param(user),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (data) {
          if(data=='sent'){
            ngDialog.open({
              template: '<p>Activation link is sent to your email address</p>',
              plain:true
            })
          }
      });
    }
    APIRestangular.all('targets').getList().then(function (data) {
      $scope.magik.targets = data;
    });
    APIRestangular.all('pricing').getList().then(function (data) {
      var pricing = {};
      pricing = data[0];
      $scope.currentPlan = $scope.magik.user.plan;
      $scope.magik.currentpricing = pricing[$scope.currentPlan];
      //console.log($scope.magik.currentpricing);
    });
    $scope.uploadImage = function () {
      $('#file').click();
    };
    $scope.openEditor = function (path) {
      $window.open('editor#/' + path);
    };
    $scope.uploader = new FileUploader({
      filters: [{
          name: 'name',
          fn: function (item) {
            return true;
          }
        }],
      formData: [{
            upload_preset: config.uploadPreset,
            tags: 'testgroup'
      }
      ],
      url: config.uploadUrl,
      isHTML5: true
    });
    //analysis phase
    $scope.analyse = function () {
      $scope.uploader.queue[0].upload();
      $rootScope.isAnalysing = true;
      $state.go('studio.analysis');
    };
    $scope.uploader.onProgressAll = function (progress) {
      $rootScope.analysisProgress = progress;
      $scope.$apply();
    };
    $scope.uploader.onAfterAddingFile = function (file) {
      if (file.uploader.queue[0].file.type == 'image/jpeg' || file.uploader.queue[0].file.type == 'image/png') {
        $scope.uploader.queue[0].upload();
        $rootScope.isAnalysing = true;
        $state.go('studio.analysis');
      } else {
        //alert('File Not supported');
        Notification.error('Unsupported file format. Please upload only .jpg/.png file');
      }
    };
    $scope.uploader.onCompleteItem = function (fileItem, response, status, headers) {
      var newtarget = {};
      newtarget.imgurl = response.secure_url;
      newtarget.public_id = response.public_id;
      newtarget.name = 'Magik-Untitled1';
      APIRestangular.all('targets').post(newtarget).then(function (response) {
        $rootScope.isAnalysing = false;
        //console.log(response);
        $scope.magik.currentTargetid = response.insertedIds[0];
        APIRestangular.all('targets').getList().then(function(response){
          $scope.magik.targets = response;
          //$state.go('studio.edit({id: })')
          $scope.uploader.clearQueue();
        });
      });
    };
    
    $scope.magik.publish = function (data) {
      var img = new Image();
      img.onload = function () {
        data.targetWidth = this.width;
      };
      img.src = data.imgurl;  // //console.log($scope.localImageSize);
      
      if (!$scope.magik.user.livecount) {
        $scope.magik.user.livecount = 0;
      }
      if (!$scope.magik.user.revisionscount) {
        $scope.magik.user.revisionscount = 0;
      }
      if (!data.livepublishing) {
        data.livepublishing = 0;
      }
      prompt({
        title: 'Publish this Magik?',
        message: 'Are you sure you want to do that?'
      }).then(function(){
          if ($scope.magik.user.livecount < $scope.magik.currentpricing.live && $scope.magik.user.revisionscount < $scope.magik.currentpricing.revisions && data.livepublishing < 30 && $scope.magik.user.active==true) {
          data.published = 'requesting';
          $rootScope.modalInstance = $modal.open({
            templateUrl: config.basePath + 'modules/studio/views/publishingModal.html',
            size: 'lg',
            backdrop: true
          });
          //console.log('Proceeding with live');
          var metadata = {};
          if(data.texthotspot){
            metadata.htmlpresent = true;
            metadata.texthotspot = data.texthotspot;
          }
          if (data.augmenttype == 'image') {
            metadata.augmenttype = 'image';
            metadata.imagehotspot = data.imagehotspot;
          }
          if (data.augmenttype == 'video') {
            //console.log(data.videohotspot);
            metadata.augmenttype = 'video';
            data.videohotspot.top = "40%";
            data.videohotspot.left="40%";
            metadata.videohotspot = data.videohotspot;
          }
          data.livepublishing = data.livepublishing + 1;
          $scope.publishData = {};
          $scope.publishData.name = data.name;
          $scope.publishData.id = data._id;
          $scope.publishData.publishcount = data.publishcount + 1;
          $scope.publishData.width = data.targetWidth;
          $scope.publishData.image = data.imgurl;
          $scope.publishData.public_id = data.public_id;
          $scope.publishData.metadata = JSON.stringify(metadata);
          ////console.log($scope.publishData.metadata);
          $scope.publishData.published = 'requesting';
          ////console.log($scope.publishData.metadata);
          if (!data.publishcount) {
            VwsRestangular.all('targets').post($scope.publishData).then(function (response) {
              //console.log(response);
            });
            data.put().then(function (response) {
              APIRestangular.all('targets').getList().then(function(response){
                $scope.magik.targets = response;
              });
            });
          } else {
          }
        } else {
          if (data.livepublishing > 2) {
            ////console.log(data);
            ngDialog.open({ template: 'multiple-publish-error' });
            ////console.log('Can process only 2 request at a time. Please wait for previous requests to complete');
            //alert('Can process only 2 request at a time. Please wait for previous requests to complete');
          } else if($scope.magik.user.active == false){
            ngDialog.open({ template: 'activate-error' });
          } 
          else{
            /*
            //console.log('Upgrade your plan');
            alert("Upgrade your plan");*/
            ngDialog.open({ template: 'upgrade-error' });
          }
        }
      });
    };
    $scope.deleteTarget = function (id) {
      var data = {};
      data = $filter('filter')($scope.magik.targets, function (d) {
        return d._id == id;
      })[0];
      //console.log(data);
      prompt({
        title: 'Delete this Magik?',
        message: 'Are you sure you want to do that?'
      }).then(function(){
        //he hit ok and not cancel
        ngDialog.open({ template: 'delete-magik' });
        if (data.vwsid) {
          $http({
            method: 'DELETE',
            url: 'vws/targets/' + data.vwsid
          });
        }
        data.remove().then(function (response) {
          //console.log(response);
          $state.go('studio.list');
          APIRestangular.all('targets').getList().then(function (data) {
            $scope.magik.targets = data;
            ngDialog.close({ template: 'delete-magik' });
          });
        });
      });
    };

    $scope.deleteVwsTarget = function (data) {
      //console.log(data);
      $http({
        method: 'DELETE',
        url: 'vws/targets/' + data.vwsid
      });
      delete data.published;
      data.put().then(function (response) {
        //console.log(response);
      });
    };

    $scope.showDuplicate = function(){
      prompt({
        title: 'Duplicate Magik',
        message: 'The Magik you tried to publish is already taken by someone else. Please try to change the magik target / create new magik'
      });
    }
  }
]);
angular.module('mang').directive('imageOption', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      $(element).on('click', function () {
        $('.template-images').removeClass('thumbnail-active');
        ////console.log($(element).hasClass("thumbnail-active"));
        if ($(this).hasClass('thumbnail-active')) {
          $(this).removeClass('thumbnail-active');
        } else {
          $(this).addClass('thumbnail-active');
        }
      });
    }
  };
});
angular.module('mang').directive('videoOption', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      //console.log('pk');
      $(element).on('click', function () {
        $('.video-thumbnail').removeClass('video-thumbnail-active');
        //console.log('wow');
        ////console.log($(element).hasClass("thumbnail-active"));
        if ($(this).hasClass('video-thumbnail-active')) {
          //console.log('yes');
          $(this).removeClass('video-thumbnail-active');
        } else {
          //console.log('no');
          $(this).addClass('video-thumbnail-active');
        }
      });
    }
  };
});
angular.module('mang').directive('textHotspotDraggable', function () {
  return {
    link: function (scope, element, attrs) {
      $(element).draggable({
        cursor: 'move',
        stop: function (event, ui) {
          //console.log(ui);
          //console.log(scope);
          //console.log($('.hotspotEditor').width());
          scope.$parent.local.magikTarget.texthotspots.left = ui.position.left * 100 / $('.hotspotEditor').width() + '%';
          scope.$parent.local.magikTarget.texthotspots.top = ui.position.top * 100 / $('.hotspotEditor').height() + '%';
          scope.$parent.local.magikTarget.put().then(function (response) {
            //console.log(response);
          });
          scope.$apply();
        }
      });
    }
  };
});
angular.module('mang').directive('textHotspotResizable', function () {
  return {
    link: function (scope, element, attrs) {
      //console.log('resizing');
      $(element).resizable({
        handles: 'n, e, s, w, ne, nw, se, sw',
        stop: function (event, ui) {
          scope.$parent.local.magikTarget.texthotspots.left = ui.position.left * 100 / $('.hotspotEditor').width() + '%';
          scope.$parent.local.magikTarget.texthotspots.top = ui.position.top * 100 / $('.hotspotEditor').height() + '%';
          scope.$parent.local.magikTarget.texthotspots.width = $(event.target).width() * 100 / $('.hotspotEditor').width() + '%';
          scope.$parent.local.magikTarget.texthotspots.height = $(event.target).height() * 100 / $('.hotspotEditor').height() + '%';
          ////console.log(scope.magikTarget);
          scope.$parent.local.magikTarget.put().then(function (response) {
            
          });
          scope.$apply();
        }
      });
    }
  };
});
angular.module('mang').directive('imageHotspotDraggable', function () {
  return {
    link: function (scope, element, attrs) {
      $(element).draggable({
        cursor: 'move',
        stop: function (event, ui) {
          scope.local.magikTarget.imagehotspot.left = ui.position.left * 100 / $('.hotspotEditor').width() + '%';
          scope.local.magikTarget.imagehotspot.top = ui.position.top * 100 / $('.hotspotEditor').height() + '%';
          
          scope.local.magikTarget.put().then(function (response) {
          });
          
          scope.$apply();
        }
      });
    }
  };
});
angular.module('mang').directive('imageHotspotResizable', function () {
  return {
    link: function (scope, element, attrs) {
      //console.log('resizing');
      $(element).resizable({
        handles: 'n, e, s, w, ne, nw, se, sw',
        stop: function (event, ui) {
          
          scope.local.magikTarget.imagehotspot.left = ui.position.left * 100 / $('.hotspotEditor').width() + '%';
          scope.local.magikTarget.imagehotspot.top = ui.position.top * 100 / $('.hotspotEditor').height() + '%';
          scope.local.magikTarget.imagehotspot.width = $(event.target).width() * 100 / $('.hotspotEditor').width() + '%';
          scope.local.magikTarget.imagehotspot.height = $(event.target).height() * 100 / $('.hotspotEditor').height() + '%';
          ////console.log(scope.magikTarget);
          scope.local.magikTarget.put().then(function (response) {
            ////console.log(response);
          });
          
          scope.$apply();
        }
      });
    }
  };
});

angular.module('mang').directive('backgroundLoad',function(){
  return{
    link:function(scope,element,attrs){
      $(element).css("background","url("+scope.preloader+")");
      $(element).resizable({});
      
    }
  }
});