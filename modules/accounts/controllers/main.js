angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('accounts', {
      url: '/accounts',
      templateUrl: config.config.basePath + 'modules/accounts/views/accounts.html',
      controller: 'AccountsController'
    });
  }
]);
angular.module('mang').controller('AccountsController', [
  '$scope',
  '$http',
  '$state',
  'APIRestangular',
  'TestRestangular',
  '$rootScope',
  '$location',
  function ($scope, $http, $state, APIRestangular, TestRestangular, $rootScope, $location) {
    console.log('AccountsController page');
    $scope.data = {};
    APIRestangular.all('users').getList({
      'email': 'iamssurya@gmail.com',
      'password': 'surya'
    }).then(function (response) {
      console.log(response);
    });
  }
]);