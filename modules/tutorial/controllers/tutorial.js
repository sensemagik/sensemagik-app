'use strict';
angular.module('mang').config([
  '$stateProvider',
  'configProvider',
  function ($stateProvider, config) {
    $stateProvider.state('tutorial', {
      url: '/tutorial',
      templateUrl: config.config.basePath + 'modules/tutorial/views/tutorial.html',
      controller: 'TutorialController'
    });
  }
]);
angular.module('mang').controller('TutorialController', [
  '$scope',
  '$http',
  '$state',
  '$rootScope',
  '$filter',
  '$sce',
  '$modal',
  '$log',
  'config',
  '$location',
  'Notification',
  function ($scope, $http, $state, $rootScope, $filter, $sce, $modal, $log, config, $location, Notification) {
    $rootScope.currentState = 'tutorial';
    $scope.$on('$viewContentLoaded', function(event){
      console.log('content loaded!');
      $('body').removeClass('blurbg');
    });
    console.log(config);
    $scope.basePath = config.basePath;
    console.log($scope.basePath);
  }
]);